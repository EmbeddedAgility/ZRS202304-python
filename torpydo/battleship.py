import random
import os
import colorama
import platform
from playsound import playsound

from colorama import Fore, Back, Style
from torpydo.ship import Color, Letter, Position, Ship
from torpydo.game_controller import GameController
from torpydo.telemetryclient import TelemetryClient

print("Starting")

myFleet = []
enemyFleet = []
field_rows = 8
field_lines = 8

def main():
    TelemetryClient.init()
    TelemetryClient.trackEvent('ApplicationStarted', {'custom_dimensions': {'Technology': 'Python'}})
    colorama.init()
    print(Fore.YELLOW + r"""
                                    |__
                                    |\/
                                    ---
                                    / | [
                             !      | |||
                           _/|     _/|-++'
                       +  +--|    |--|--|_ |-
                     { /|__|  |/\__|  |--- |||__/
                    +---------------___[}-_===_.'____                 /\
                ____`-' ||___-{]_| _[}-  |     |_[___\==--            \/   _
 __..._____--==/___]_|__|_____________________________[___\==--____,------' .7
|                        Welcome to Battleship                         BB-61/
 \_________________________________________________________________________|""" + Style.RESET_ALL)

    while True:
        initialize_game()
        start_game()

        new_game = False
        while not new_game:
            val = input('Do you want to start a new game? [Y/N]')
            if(val == 'Y'):
                new_game = True
                break

def start_game():
    global myFleet, enemyFleet

    # clear the screen
    if(platform.system().lower()=="windows"):
        cmd='cls'
    else:
        cmd='clear'   
    os.system(cmd)
    print(r'''
                  __
                 /  \
           .-.  |    |
   *    _.-'  \  \__/
    \.-'       \
   /          _/
   |      _  /
   |     /_\
    \    \_/
     """"""""''')

    while True:
        print()
        print("=======================================================")
        print("Player, it's your turn")

        while True:
            position = parse_position(input("Enter coordinates for your shot (A-H)(1-8):"))

            if position is None:
                print("Invalid position! Try again \n")
            else:
                break

        is_hit = GameController.check_is_hit(enemyFleet, position)
        if is_hit:
            print(r'''
                \          .  ./
              \   .:"";'.:..""   /
                 (M^^.^~~:.'"").
            -   (/  .    . . \ \)  -
               ((| :. ~ ^  :. .|))
            -   (\- |  \ /  |  /)  -
                 -\  \     /  /-
                   \  \   /  /''')

        print(produceShootConsoleText(is_hit))
        print(get_status(enemyFleet))

        TelemetryClient.trackEvent('Player_ShootPosition', {'custom_dimensions': {'Position': str(position), 'IsHit': is_hit}})
        if check_if_player_won(enemyFleet):
            print("You are the winner!")
            print('''
                             .---'::'        `---.
                            (::::::'              )
                            |`-----._______.-----'|
                            |              :::::::|
                           .|               ::::::!-.
                           \|               :::::/|/
                            |               ::::::|
                            |       CONGRATS     :|
                            |                 ::::|
                            |               ::::::|
                            |              .::::::|
                            |              :::::::|
                             \            :::::::/
                              `.        .:::::::'
                                `-._  .::::::-'
____________________________________|  """|"_________________________________________
                                    |  :::|
                                    |   ::|
                                   /     ::\                                        
                              __.-'      :::`-.__
                             (_           ::::::_)
                               `"""---------"""'
            ''')
            play_winning_sound()
            return
        position = get_random_position()
        is_hit = GameController.check_is_hit(myFleet, position)
        print()
        print(produceCPUShootConsoleText(position, is_hit))
        TelemetryClient.trackEvent('Computer_ShootPosition', {'custom_dimensions': {'Position': str(position), 'IsHit': is_hit}})
        if is_hit:
            print(r'''
                \          .  ./
              \   .:"";'.:..""   /
                 (M^^.^~~:.'"").
            -   (/  .    . . \ \)  -
               ((| :. ~ ^  :. .|))
            -   (\- |  \ /  |  /)  -
                 -\  \     /  /-
                   \  \   /  /''')
        print(get_status(myFleet))
        if check_if_player_won(myFleet):
            play_loosing_sound()
            print("You lost!")
            return

def check_if_player_won(fleet: list[Ship]):
    for ship in fleet:
        if len(ship.positions) != len(ship.hits):
            return False
        
    return True


def get_status(fleet: list[Ship]):
    status = 'Status of the opponent fleet:\n'
    for ship in fleet:
        if(len(ship.positions) != len(ship.hits)):
            status += f'{ship.name} of size: {ship.size}\n'
        else:
            status += f'{ship.name} of size: {ship.size} [SUNKEN]\n'
    
    return status


def parse_position(input: str):
    letter_options = [letter.name for letter in Letter]
    letter_str = input.upper()[:1]

    if letter_str not in letter_options:
        return None

    letter = Letter[letter_str]
    number = int(input[1:])

    if number > field_rows:
        return None

    return Position(letter, number)

def get_random_position():
    letter = Letter(random.randint(1, field_lines))
    number = random.randint(1, field_rows)
    position = Position(letter, number)

    return position

def initialize_game():
    initialize_myFleet()
    quick_game = input('Do you want to play quick game with enemy having one Patrol boat? [Y/N]')
    if(quick_game == 'Y'):
        initialize_enemyFleet_for_testing()
    else:
        initialize_enemyFleet()


    


def produceShootConsoleText(is_hit:bool):
    if(is_hit):
        return "\033[32mYeah ! Nice hit !\033[0m"
    return "\033[31mMiss\033[0m"

def produceCPUShootConsoleText(position: Position, is_hit:bool):
    if(is_hit):
        return f"\033[31mComputer shoot in {str(position)} and hit your ship!\033[0m"
    return f"\033[32mComputer shoot in {str(position)} and missed!\033[0m";

def initialize_myFleet():
    global myFleet

    myFleet = GameController.initialize_ships()

    print("Please position your fleet (Game board has size from A to H and 1 to 8) :")

    for ship in myFleet:
        while(is_ship_initialized(ship) == False):
            print()
            print(f"Please enter the positions for the {ship.name} (size: {ship.size})")
            for i in range(ship.size):
                position_input = input(f"Enter position {i+1} of {ship.size} (i.e A3):")
                if(check_if_position_already_used(position_input, myFleet)):
                    print("Position already used, please enter position {i+1} of {ship.size} (i.e A3):!")
                    i = i - 1
                    continue
                if(check_if_position_is_illegal(position_input)):
                    print("Position is out of bounds, please enter position {i+1} of {ship.size} (i.e A3):!")
                    i = i - 1
                    continue
                ship.add_position(position_input)
                TelemetryClient.trackEvent('Player_PlaceShipPosition', {'custom_dimensions': {'Position': position_input, 'Ship': ship.name, 'PositionInShip': i}})
            if(is_ship_initialized(ship) == False):
                print("Ship is not of correct size, please retry entering the ship.")
            if(is_ship_vertical_or_horizontal(ship) == False):
                print("Ship is not vertical or horizontal, please retry entering the ship.")

def is_ship_initialized(ship: Ship):
    if len(ship.positions) == ship.size:
        return True
    return False

def is_ship_vertical_or_horizontal(ship: Ship):
    columns = [pos.columns for pos in ship.positions]
    rows = [pos.rows for pos in ship.positions]
    if len(set(columns)) == 1 or len(set(rows)) == 1:
        return True
    return False

def check_if_position_is_illegal(position_input: Position):
    if position_input[0].upper() not in [letter.name for letter in Letter]:
        return True
    if int(position_input[1:]) not in range(1, 9):
        return True
    return False     

def check_if_position_already_used(position_input: Position, _myFleet: list[Ship]):
    for _ship in _myFleet:
        for _position in _ship.positions:
            if position_input == str(_position):
                return True
    return False

def initialize_enemyFleet():
    global enemyFleet

    enemyFleet = GameController.initialize_ships_for_testing()

    enemyFleet[0].positions.append(Position(Letter.B, 4))
    enemyFleet[0].positions.append(Position(Letter.B, 5))
    # enemyFleet[0].positions.append(Position(Letter.B, 6))
    # enemyFleet[0].positions.append(Position(Letter.B, 7))
    # enemyFleet[0].positions.append(Position(Letter.B, 8))

    enemyFleet[1].positions.append(Position(Letter.E, 5))
    enemyFleet[1].positions.append(Position(Letter.E, 6))
    enemyFleet[1].positions.append(Position(Letter.E, 7))
    enemyFleet[1].positions.append(Position(Letter.E, 8))

    # enemyFleet[2].positions.append(Position(Letter.A, 3))
    # enemyFleet[2].positions.append(Position(Letter.B, 3))
    # enemyFleet[2].positions.append(Position(Letter.C, 3))

    # enemyFleet[3].positions.append(Position(Letter.F, 8))
    # enemyFleet[3].positions.append(Position(Letter.G, 8))
    # enemyFleet[3].positions.append(Position(Letter.H, 8))

    # enemyFleet[4].positions.append(Position(Letter.C, 5))
    # enemyFleet[4].positions.append(Position(Letter.C, 6))


def initialize_enemyFleet_for_testing():
    global enemyFleet

    enemyFleet = GameController.initialize_ships_for_testing()

    enemyFleet[0].positions.append(Position(Letter.A, 1))
    enemyFleet[0].positions.append(Position(Letter.A, 2))

def play_winning_sound():
    dirname = os.path.dirname(__file__)
    filename = os.path.join(dirname, 'winning-sound.wav')
    playsound(filename)


def play_loosing_sound():
    dirname = os.path.dirname(__file__)
    filename = os.path.join(dirname, 'sad_trombone.mp3')
    playsound(filename)


if __name__ == '__main__':
    main()
