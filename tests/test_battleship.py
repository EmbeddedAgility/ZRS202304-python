import unittest

from torpydo.battleship import parse_position, produceShootConsoleText, produceCPUShootConsoleText
from torpydo.battleship import enemyFleet
from torpydo.ship import Ship, Color, Position, Letter
from torpydo.battleship import check_if_player_won, get_status

class TestBattleship(unittest.TestCase):

    def test_parse_position_valid(self):
        self.assertTrue(parse_position("A1"))
        self.assertTrue(parse_position("B5"))
        self.assertTrue(parse_position("C7"))
        self.assertTrue(parse_position("H6"))

    def test_parse_position_invalid(self):
        self.assertFalse(parse_position("X9"))
        self.assertFalse(parse_position("Y7"))        
        self.assertFalse(parse_position("Z4"))

    def test_parse_user_shoot_output_hit(self):
        self.assertEqual(produceShootConsoleText(True), "\033[32mYeah ! Nice hit !\033[0m")
        
    def test_parse_user_shoot_output_miss(self):
        self.assertEqual(produceShootConsoleText(False), "\033[31mMiss\033[0m")

    def test_parse_user_shoot_output_hit(self):
        position = Position(Letter.A, 1)
        self.assertEqual(produceCPUShootConsoleText(position,True), f"\033[31mComputer shoot in {str(position)} and hit your ship!\033[0m")
        
    def test_parse_user_shoot_output_miss(self):
        position = Position(Letter.A, 1)
        self.assertEqual(produceCPUShootConsoleText(position,False), f"\033[32mComputer shoot in {str(position)} and missed!\033[0m")

    def test_game_ends_win(self):
        fleet = [Ship('Test_1', 1, Color.CADET_BLUE)]
        fleet[0].positions = [Position(Letter.A, 1), Position(Letter.A, 2)]
        fleet[0].hits = [Position(Letter.A, 1), Position(Letter.A, 2)]
        self.assertTrue(check_if_player_won(fleet))


    def test_game_continues(self):
        fleet = [Ship('Test_1', 1, Color.CADET_BLUE), Ship('Test_1', 1, Color.CADET_BLUE)]
        fleet[0].positions = [Position(Letter.A, 1), Position(Letter.A, 2)]
        fleet[1].positions = [Position(Letter.B, 1), Position(Letter.B, 2), Position(Letter.B, 3)]
        fleet[0].hits = [Position(Letter.A, 1), Position(Letter.A, 2)]
        self.assertFalse(check_if_player_won(fleet))


    def test_show_status(self):
        fleet = [Ship('Ship_1', 2, Color.CADET_BLUE), Ship('Ship_2', 3, Color.CADET_BLUE)]
        fleet[0].positions = [Position(Letter.A, 1), Position(Letter.A, 2)]
        fleet[1].positions = [Position(Letter.B, 1), Position(Letter.B, 2), Position(Letter.B, 3)]
        fleet[0].hits = [Position(Letter.A, 1), Position(Letter.A, 2)]
        status = get_status(fleet)
        expectedStatus = 'Status of the opponent fleet:\nShip_1 of size: 2 [SUNKEN]\nShip_2 of size: 3\n'
        self.assertEqual(status, expectedStatus)    
        

    def init_ship(ship: Ship, positions: list):
        ship.positions = positions

if '__main__' == __name__:
    unittest.main()
